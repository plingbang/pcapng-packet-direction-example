; Section Header Block

block1:
dd 0x0a0d0d0a           ; Block Type
dd block1length         ; Block Total Length
dd 0x1a2b3c4d           ; Byte-Order Magic
dw 0x0001               ; Major Version
dw 0x0000               ; Minor Version
dq 0xffffffffffffffff   ; Section Length
dd block1length         ; Block Total Length
block1length equ $ - block1

; Interface Description Block

block2:
dd 0x00000001           ; Block Type
dd block2length         ; Block Total Length
dw 0x0001               ; LinkType: LINKTYPE_ETHERNET
dw 0x0000               ; Reserved
dd 2048                 ; SnapLen
dd block2length         ; Block Total Length
block2length equ $ - block2

; Enhanced Packet Block

block3:
dd 0x00000006           ; Block Type
dd block3length         ; Block Total Length
dd 0                    ; Interface ID
dq 0                    ; Timestamp
dd 42                   ; Captured Packet Length
dd 42                   ; Original Packet Length
; Packet Data
db 0xff, 0xff, 0xff, 0xff, 0xff, 0xff   ; Ethernet Destination
db 0x00, 0x00, 0x5e, 0x00, 0x53, 0x42   ; Ethernet Source
db 0x08, 0x06                           ; Ethernet Type: ARP
db 0x00, 0x01                           ; ARP Hardware type: Ethernet
db 0x08, 0x00                           ; ARP Protocol type: IPv4
db 0x06                                 ; ARP Hardware size
db 0x04                                 ; ARP Protocol size
db 0x00, 0x01                           ; ARP Opcode: request
db 0x00, 0x00, 0x5e, 0x00, 0x53, 0x42   ; ARP Sender MAC address
db 192, 0, 2, 42                        ; ARP Sender IP address
db 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   ; ARP Target MAC address
db 192, 0, 2, 1                         ; ARP Target IP address
db 0x00, 0x00           ; padding
dw 2                    ; Option Type: epb_flags
dw 4                    ; Option Length: 4
dd 10b                  ; Enhanced Packet Block Flags Word: Outbound packet
dd block3length         ; Block Total Length
block3length equ $ - block3

; Enhanced Packet Block

block4:
dd 0x00000006           ; Block Type
dd block4length         ; Block Total Length
dd 0                    ; Interface ID
dq 0                    ; Timestamp
dd 42                   ; Captured Packet Length
dd 42                   ; Original Packet Length
; Packet Data
db 0x00, 0x00, 0x5e, 0x00, 0x53, 0x42   ; Ethernet Destination
db 0x00, 0x00, 0x5e, 0x00, 0x53, 0x01   ; Ethernet Source
db 0x08, 0x06                           ; Ethernet Type: ARP
db 0x00, 0x01                           ; ARP Hardware type: Ethernet
db 0x08, 0x00                           ; ARP Protocol type: IPv4
db 0x06                                 ; ARP Hardware size
db 0x04                                 ; ARP Protocol size
db 0x00, 0x02                           ; ARP Opcode: reply
db 0x00, 0x00, 0x5e, 0x00, 0x53, 0x01   ; ARP Sender MAC address
db 192, 0, 2, 1                         ; ARP Sender IP address
db 0x00, 0x00, 0x5e, 0x00, 0x53, 0x42   ; ARP Target MAC address
db 192, 0, 2, 42                        ; ARP Target IP address
db 0x00, 0x00           ; padding
dw 2                    ; Option Type: epb_flags
dw 4                    ; Option Length: 4
dd 01b                  ; Enhanced Packet Block Flags Word: Inbound packet
dd block4length         ; Block Total Length
block4length equ $ - block4
